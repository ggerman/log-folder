#!/bin/bash
echo "Press [CTRL+C] to stop.."
folder="/path of folder/"

size=1
files=0

while :
do
  if [[ $size -ne $size_new  ||  $files -ne $files_new ]]
  then
    size_new=$(du -k $folder | cut -f 1)
    size=$size_new
    files_new=$(ls -lh $folder | wc -l)
    echo "Number of files: $files_new - Size of folder: $size_new"
    files_new=$(ls -lh $folder | wc -l)
    files=$files_new
  else
    size_new=$(du -k $folder | cut -f 1)
    files_new=$(ls -lh $folder | wc -l)
    echo -n "."
  fi

  sleep 1

done
